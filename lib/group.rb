# frozen_string_literal: true

require_relative 'category'
require 'yaml'

module Gitlab
  module Homepage
    class Group
      LABEL_PREFIX = 'group::'

      attr_reader :key

      def initialize(key, data, stage: nil)
        @key = key
        @data = data
        @stage = stage
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      def group_table
        partial('includes/product/group_table', locals: { group_display: self })
      end

      def categories
        @data['categories'] || []
      end

      def label
        @data['label'] || "#{LABEL_PREFIX}#{key.tr('_', ' ')}"
      end

      def slack
        @data.dig('slack', 'channel')
      end

      ##
      # Middeman Data File objects compatibility
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.reset_all!
        @all = nil
      end

      def self.all!
        @all ||= YAML.load_file(File.expand_path('../data/stages.yml', __dir__))['stages'].flat_map do |stage_key, stage_data|
          stage_data['groups'].map do |group_key, group_data|
            group_data['section'] = stage_data['section']
            group_data['stage'] = stage_key
            new(group_key, group_data)
          end
        end
      end

      def self.ssot
        all!.each_with_object({}) do |group, memo|
          stage = Stage.all!.find { |stage| stage.key == group.stage }
          product_manager = members_by_names(group.pm)
          engineering_manager = members_by_names(group.em)
          backend_engineering_manager = members_by_names(group.backend_engineering_manager)
          frontend_engineering_manager = members_by_names(group.frontend_engineering_manager)
          product_design_managers = members_by_names(group.pm || stage.pdm)
          backend_engineers = Gitlab::Homepage.team.department_matches(department: group.be_team_tag, member_type: "person")
          frontend_engineers = Gitlab::Homepage.team.department_matches(department: group.fe_team_tag, member_type: "person")
          software_engineers_in_test = members_by_names(group.sets)
          product_designers = members_by_names(group.ux)
          technical_writers = members_by_names(group.tech_writer)
          support_engineers = members_by_names(group.support)

          memo[group.key.to_sym] = {
            name: group.name,
            section: group.section,
            stage: group.stage,
            categories: group.categories,
            label: group.label,
            slack_channel: group.slack,
            product_managers: member_usernames(product_manager),
            engineering_managers: member_usernames(engineering_manager),
            backend_engineering_managers: member_usernames(backend_engineering_manager),
            frontend_engineering_managers: member_usernames(frontend_engineering_manager),
            technical_writers: member_usernames(technical_writers),
            support_engineers: member_usernames(support_engineers),
            backend_engineers: member_usernames(backend_engineers),
            frontend_engineers: member_usernames(frontend_engineers),
            product_design_managers: member_usernames(product_design_managers),
            software_engineers_in_test: member_usernames(software_engineers_in_test),
            product_designers: member_usernames(product_designers),
            triage_ops_config: group.triage_ops_config
          }
        end
      end

      def self.members_by_names(names)
        names = Array(names).compact
        names.delete('TBD')

        return [] if names.empty?

        members = names.filter_map do |name|
          Team::Member.find_by_name(name.delete_suffix(' (Interim)'))
        end

        puts "[WARNING] No members matching #{names}!" if members.empty?

        members
      end

      def self.member_usernames(members)
        members = Array(members).compact
        return [] if members.empty?

        usernames = members.filter_map do |member|
          member.username if member.respond_to?(:username)
        end

        puts "[WARNING] No usernames matching #{members}!" if usernames.empty?

        usernames
      end
    end
  end
end
